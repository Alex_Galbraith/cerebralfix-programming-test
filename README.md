# README #
## How To Run ##
Either open the title screen scene in unity and play, build the game to an Android device, or install from the APK in the 'Distributions' folder.
I would have done a WebGL build but my website is a mess and to be honest with you, I dont remember how to upload a build to it.

## How To Play ##
Move the mouse or touch the screen to move the paddle. Double tap or double click to boost the ball. Break all the bricks, dont lose all your balls 3 times. Your lives
are displayed on the paddle.

## Known Issues ##
It is possible for the ball to get forced out of the arena. I would need to do a fair bit of phsysics debugging to fix this. The physics is custom because initially I wanted
the double click ability to function similar to the high speed ball in the game Lethal League, but that turned out not to be fun without doing a lot more work.

Obtuse angle collisions with the paddle may cause the ball to slip off the paddle. This is simply due to the collision normals not respecting the actual visual geometry of the paddle. Some tweaking would fix this.

## Programming Philosophy ##
This whole game is massively over engineered. It is always an odd task in these programming tests to display future proofing for a game that has no future and no design for the future. As such, I picked a few hypothetical design problems:

* Support for power ups. Using interfaces and managers allows us to swap the balls out at a later date. Ideally we would have some kind of DI and factories to generate
or swap between balls of different types.

* Support for reskinning. By separating display and logic we can just change the display code and prefabs.

* Support for more interesting ball interactions. By providing an interface for interacting with the ball physics we could easily add spin mechanics or pass-through blocks or any number of funky interactions between paddle, terrain, and balls.

* Support for multiple player or control schemes. By abstracting input into ScriptableObject instances it is very easy to add new paddles that are controlled differently. The balls' response to player input
is a little more complex and would need a further layer of abstraction added.

At the end of the day, this was all rather rushed and it doesnt actually fulfil these goals all that well, but the thought is there.



# Instructions #

### Breakout ###
We are going old school and want to test your skills at creating a Breakout game.  There are a number of breakout style games available on the internet for you to test out.
Don't spend too long on this. 3-4 hours is more than enough.

### Functional Requirements ###
1. The ball should rebound off the sides, top of the game board and off the paddle. 
2. If the ball falls off the bottom of the board the game is over or a life is lost.
3. The game should play from the Unity Editor.
4. The game should be re-playable without closing the game.

### What we're looking for ###
* The process you used to complete this test
* Prudent use of Object Orientated design
* Code reusability
* Extensibility and maintainability of the software
* Use of appropriate Unity capabilities
* Use of best practises
* Your creativity in making the game fun

### Deliverables ###
* Instructions on how to run and play your game
* Code should be committed to a git repository hosted on [bitbucket.org](https://bitbucket.org)

### Extra Credit ###
* Can be played in both orientations; landscape and portrait
* Can be installed as an app
* Any extra features you want to include in the game

## Technology ##
You can use assets (excluding code) from the Unity Asset Store, however please note what assets you have used and why you have used them.

## How do I get set up? ##
* [Fork](../../fork) this repository to your bitbucket account as a private repository, then clone it to your local machine
* Install Unity 2019.4 and create a new Unity project in the repository.

## Next Steps ##
After you have finished your submission, make sure the reviewers have read access to your repository. Our developers will review your submission and then invite you in for a discussion.