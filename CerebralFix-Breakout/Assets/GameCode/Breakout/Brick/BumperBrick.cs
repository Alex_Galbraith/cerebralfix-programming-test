using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperBrick : Brick, IBallCollisionAffector
{
    [SerializeField]
    protected float _bumpPower = 3;
    public event Action<IBrick> OnDestroyedEvent;


    public override BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D)
    {
        base.TriggerDestroy();
        return new BallCollisionParams(hit2D){HitPower = _bumpPower};
    }
}
