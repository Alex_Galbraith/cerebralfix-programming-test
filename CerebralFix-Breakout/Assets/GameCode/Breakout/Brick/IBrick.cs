using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBrick : IGameObject
{
    event Action<IBrick> OnDestroyedEvent;
}
