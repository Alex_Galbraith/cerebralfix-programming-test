using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusBallBrick : Brick, IBallCollisionAffector
{
    [SerializeField]
    protected BallManager _ballManager;
    [SerializeField]
    protected float _spawnVelocity = 5f;

    protected bool _destroyed;

    public override BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D)
    {
        base.TriggerDestroy();
        _ballManager.SpawnBall(transform.position, Vector2.down * _spawnVelocity);
        return new BallCollisionParams(hit2D);
    }

    private void OnValidate() {
        if(!_ballManager)
            _ballManager = FindObjectOfType<BallManager>();
    }
}
