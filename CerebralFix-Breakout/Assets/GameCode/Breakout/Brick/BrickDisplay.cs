using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickDisplay : MonoBehaviour
{
    [SerializeField]
    Brick _brick;
    [SerializeField]
    Animation _animation;
    // Start is called before the first frame update
    void Start()
    {
        _brick.OnDestroyedEvent += AnimatedDestroy;
    }

    private void AnimatedDestroy(IBrick obj)
    {
        _animation.Play();
    }

    public void OnAnimationFinished()
    {
        Destroy(_brick.gameObject);
    }

    private void Reset() {
        _brick = GetComponent<Brick>();
    }

  
}
