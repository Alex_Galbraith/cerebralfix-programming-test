using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Theres a bunch of code duplication between here and the brickManager that would be fairly easy to 
//genericise, but A) I cant be bothered and B) its likely the the way these two classes would evolve would result in them becoming separate
//again in future.

public class BrickManager : MonoBehaviour
{
 
    public event Action OnAllBricksDestroyed;
    protected List<IBrick> _bricks;

    private void Awake() {
        UpdateBrickList();
    }
    
    private void Start() {
        foreach(IBrick brick in _bricks)
        {
            brick.OnDestroyedEvent += HandleBrickDestroyed;
        }
    }

    private void HandleBrickDestroyed(IBrick brick)
    {
        _bricks.Remove(brick);
        brick.OnDestroyedEvent -= HandleBrickDestroyed;
    }


    //Run this in late update so that if a brick is destroyed then created in the same frame
    //we dont end the game prematurely
    private void LateUpdate() {
        if (_bricks.Count == 0)
        {
            OnAllBricksDestroyed?.Invoke();
        }
    }

    protected void UpdateBrickList()
    {
        _bricks = new List<IBrick>(GetComponentsInChildren<IBrick>());
    }
}
