using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour, IBrick, IBallCollisionAffector
{
    [SerializeField]
    protected Collider2D _collider;
    public event Action<IBrick> OnDestroyedEvent;

    protected void TriggerDestroy()
    {
        _collider.enabled = false;
        OnDestroyedEvent?.Invoke(this);
    }

    public virtual BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D)
    {
        TriggerDestroy();
        return new BallCollisionParams(hit2D);
    }

    private void OnValidate() {
        if(!_collider)
            _collider = GetComponent<Collider2D>();
    }
}
