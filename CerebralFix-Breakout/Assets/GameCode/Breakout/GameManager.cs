using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    protected BrickManager _brickManager;
    [SerializeField]
    protected BallManager _ballManager;
    [SerializeField]
    private int _startingLives = 3;
    [SerializeField]
    protected Transform _ballSpawnPos;
    [SerializeField]
    protected float _startingBallSpeed = 10;
    private int _currentLives;

    public int CurrentLives
    {
        get => _currentLives;
        protected set
        {
            _currentLives = value;
            OnLivesUpdated?.Invoke(_currentLives);
        }
    }

    public int StartingLives  => _startingLives;

    public event Action<int> OnLivesUpdated;
    public event Action OnLoss;
    public event Action OnWin;

    void Awake()
    {
        _brickManager.OnAllBricksDestroyed += HandleWin;
        _ballManager.OnAllBallsDestroyed += HandleLifeLoss;
    }

    private IEnumerator Start()
    {
        CurrentLives = _startingLives;
        _ballManager.enabled = false;
        //Wait a mo for the player to wake up
        yield return new WaitForSeconds(1f);
        SpawnStartingBall();
        _ballManager.enabled = true;
    }

    private void HandleLifeLoss()
    {
        CurrentLives--;
        if (CurrentLives == 0)
        {
            Lose();
        }
        else
        {
            SpawnStartingBall();
        }
    }

    private void SpawnStartingBall()
    {
        _ballManager.SpawnBall(_ballSpawnPos.position, Vector2.down * _startingBallSpeed);
    }

    private void Lose()
    {
        OnLoss?.Invoke();
        Debug.Log("Lose!");
        //Cheap hack so we dont recieve any more events
        _brickManager.OnAllBricksDestroyed -= HandleWin;
        _ballManager.OnAllBallsDestroyed -= HandleLifeLoss;

    }

    private void HandleWin()
    {
        OnWin?.Invoke();
        Debug.Log("Win!");
        //Cheap hack so we dont recieve any more events
        _brickManager.OnAllBricksDestroyed -= HandleWin;
        _ballManager.OnAllBallsDestroyed -= HandleLifeLoss;
    }

    public void RestartLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(gameObject.scene.name, LoadSceneMode.Single);
    }

}
