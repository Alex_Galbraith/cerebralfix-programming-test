using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour, IBallCollisionAffector
{
    public BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D)
    {
        ball.FlagOutOfBounds();
        return new BallCollisionParams(hit2D);
    }
}
