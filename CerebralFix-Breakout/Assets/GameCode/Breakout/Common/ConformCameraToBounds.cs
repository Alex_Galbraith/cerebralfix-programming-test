using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Resizes an ORTHOGRAPHIC camera to fit a bounding box. 
/// Doesnt currently reposition.
/// 
/// This code comes from a snippet I wrote for prototyping. Its a handy wee script that provides a quick
/// way to handle multiple resolutions
/// </summary>
public class ConformCameraToBounds : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    //Hacky use of a BoxCollider2D. Would be better to use a custom class with a Gizmo
    [SerializeField]
    private BoxCollider2D _boundingBox;
    [SerializeField]
    private bool _fillScreen;
    //Game state isnt always set up on game start so you might have to add a delay
    [SerializeField]
    private float _delay;

    /// <summary>
    /// True if the screen should be filled by the bounding box, false if the bounding box should fit in the screen.
    /// Setting will cause a recalculation of scaling
    /// </summary>
    /// <value></value>
    public bool FillScreen { 
        get => _fillScreen; 
        set{
            _fillScreen = value;  
            Rescale();
        }
    }

    /// <summary>
    /// Setting will cause a full recalculation of scaling
    /// </summary>
    public BoxCollider2D BoundingBox { 
        get => _boundingBox; 
        set{
            _boundingBox = value;
            Rescale();
        }
    }
    
    /// <summary>
    /// Setting will cause a full recalculation of scaling
    /// </summary>
    public Camera Camera
    {
        get => _camera; 
        set{
            _camera = value;
            Rescale();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Delay so Screen.safeArea can get set up
        if(_delay == 0)
            Rescale();
        else
            Invoke("Rescale", _delay);
    }

    public void Rescale(){
        //work out how big our bounding box is on screen
        Vector2 zero = _camera.WorldToScreenPoint(Vector3.zero);
        Vector2 len = _camera.WorldToScreenPoint(_boundingBox.size);
        Vector2 delta = len - zero;
        Vector2 safeArea = Screen.safeArea.size;
        //how much of the screen does the bounding box take up a fraction of the whole screen
        float xRatio = Mathf.Abs(delta.x)  / Screen.safeArea.width;
        float yRatio = Mathf.Abs(delta.y)  / Screen.safeArea.height;
        //get our most constraining axis to fit, or least constraining axis if we want to fill the screen
        float concernedRation = _fillScreen ? Mathf.Min(xRatio, yRatio) : Mathf.Max(xRatio, yRatio);
        //zoom in/out so the bounding box is contained in the screen
        _camera.orthographicSize = _camera.orthographicSize * concernedRation;
        Debug.Log($"#CAMERA# Ortho size set: {_camera.orthographicSize}");
    }
}