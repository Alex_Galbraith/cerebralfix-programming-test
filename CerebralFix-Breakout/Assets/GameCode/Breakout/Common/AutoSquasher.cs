using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class AutoSquasher : MonoBehaviour
{
    [SerializeField]
    Vector2 _lastPosition;
    [SerializeField]
    Vector2 _velocity;
    [SerializeField]
    Vector2 _acceleration;

    [SerializeField]
    SquashAndStretcher _squasher;

    [SerializeField]
    float _expSpeed = 0.1f;
    [SerializeField]
    float _accScale = 1;
    [SerializeField]
    float _velScale = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!_squasher)
            return;
        UpdateDeltas();
        Vector2 stretch = _acceleration * _accScale + _velocity * _velScale; 

        float mag;
        //if we are decellerating, flip our magnitude
        if (Vector2.Dot(stretch, _velocity) <= 0) {
            //Add small value to prevent div by zero
            mag = 1/(stretch.magnitude + 0.0001f);
        }
        else {
            mag = stretch.magnitude;
        }

        _squasher.Stretch = mag * stretch.normalized;
    }

    protected void UpdateDeltas(){
        //Use exponential moving averages to calculate velocities
        Vector2 posDelta = (Vector2)transform.position - _lastPosition;
        _lastPosition = transform.position;
        Vector2 lastVel = _velocity;
        _velocity = (_expSpeed) * (posDelta * Time.deltaTime) + (1 - _expSpeed) * _velocity;
        Vector2 velDelta = _velocity - lastVel;
        _acceleration = (_expSpeed) * (velDelta * Time.deltaTime) + (1 - _expSpeed) * _acceleration;

    }
}
