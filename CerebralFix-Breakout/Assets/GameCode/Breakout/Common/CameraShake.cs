using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField]
    CameraShakeBus _shakeBus;
    private float _intensity;
    private float _duration;
    private float _initDuration;

    // Start is called before the first frame update
    void Start()
    {
        _shakeBus.OnShake += HandleShake;
    }

    private void HandleShake(float intensity, float duration)
    {
        _intensity = intensity;
        _duration = duration;
        _initDuration = duration;
    }

    // Update is called once per frame
    void Update()
    {
        if(_duration > 0)
        {
            _duration -= Time.unscaledDeltaTime;
            //If we want to move the camera, we can just place the camera inside a parent and move the parent
            transform.localPosition = UnityEngine.Random.insideUnitSphere * _intensity * _duration / _initDuration;
        }
    }

    private void OnDestroy() {
        _shakeBus.OnShake -= HandleShake;
    }
}
