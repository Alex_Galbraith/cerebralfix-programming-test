using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Code largely taken from one of my blog posts: http://www.alexgalbraith.nz/2019/04/05/programmatic-squash-and-stretch/
[ExecuteInEditMode]
public class SquashAndStretcher : MonoBehaviour
{ 
    [SerializeField]  
    Renderer renderer;
    [SerializeField]  
    float _maxStretch = 2;
    [SerializeField]  
    Vector3 _offset;
    [SerializeField]  
    public Vector2 Stretch;
    MaterialPropertyBlock _block;
    private void SetSquashAndStretch(Vector2 stretch, float mag){
        mag += 1;
        //clamp our magnitude
        mag = Mathf.Clamp(mag, 1/_maxStretch, _maxStretch);
        //Calculate rotation matrix
        float angle = Mathf.Atan2(stretch.y, -stretch.x);
        Quaternion q = Quaternion.Euler(0, 0, angle/Mathf.PI * 180);
        Matrix4x4 r = Matrix4x4.Rotate(q);
        //Calculate translation matrix
        Matrix4x4 t = Matrix4x4.Translate(_offset);
        //Calculate rotation matrix inverse
        Matrix4x4 t_i = Matrix4x4.Translate(-_offset);
        //Calculate scale matrix
        Matrix4x4 s = Matrix4x4.Scale(new Vector2(mag,1/mag));
        //Create our complete stretch matrix
        //The transpose of a rotation matrix is the inverse of the same rotation matrix, thus r.transpose = r.inverse
        Matrix4x4 m = t_i * r.transpose * s * r * t ;
        //Set our matrix in shader
        renderer.GetPropertyBlock(_block);
        _block.SetMatrix("_Deform_Matrix", m);
        renderer.SetPropertyBlock(_block);
    }

    private void Update() {
        if(_block == null)
            _block = new MaterialPropertyBlock();
        //Not the most efficient but it means we can use the unity animator to animate it easily
        SetSquashAndStretch(Stretch, Stretch.magnitude);
    }
}
