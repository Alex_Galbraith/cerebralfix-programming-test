using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Camera Shake Bus")]
public class CameraShakeBus : ScriptableObject
{
    public event ShakeDelegate OnShake;
    public void ShakeCamera(float intensity, float duration){
        OnShake?.Invoke(intensity, duration);
    }
}
//Confusing without names, so made a delegate rather than Action<string,string>
public delegate void ShakeDelegate(float intensity, float duration);
