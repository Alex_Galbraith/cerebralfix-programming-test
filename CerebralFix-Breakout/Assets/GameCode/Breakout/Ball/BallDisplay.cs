using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDisplay : MonoBehaviour
{
    [SerializeField]
    CameraShakeBus _shakeBus;
    [SerializeField]
    protected BasicBall _ball;
    [SerializeField]
    Animation _animation;
    [SerializeField]
    SquashAndStretcher _squash;
    [SerializeField]
    AutoSquasher _autoSquasher;
    [SerializeField]
    private float _shakeIntensity;
    [SerializeField]
    private float _shakeDuration;
    private Action _initCallback;

    private void Reset() {
        _ball = GetComponent<BasicBall>();
    }

    private void HandleCollision(Vector2 arg1, RaycastHit2D arg2)
    {
       _shakeBus.ShakeCamera(arg1.magnitude * _shakeIntensity, _shakeDuration);
    }

    private void Awake() {
        _ball.OnInitialize += HandleInit;
        _ball.OnCollision += HandleCollision;
    }

    private void HandleInit(Action callback)
    {
        _autoSquasher.enabled = false;
        _initCallback = callback;
        _animation.Play();
    }

    public void OnAnimationComplete(){
        _autoSquasher.enabled = true;
        _initCallback?.Invoke();
    }
}
