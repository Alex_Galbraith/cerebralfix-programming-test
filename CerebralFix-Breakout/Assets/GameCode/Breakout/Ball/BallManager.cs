using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{

    [SerializeField]
    protected BasicBall _basicBallPrefab;
    /// <summary>
    /// There are no balls left, someone do something!
    /// </summary>
    public event Action OnAllBallsDestroyed;
    protected List<IBall> _balls;

    private void Awake() {
        UpdateBallList();
    }

    private void Start() {
        foreach(IBall ball in _balls)
        {
            ball.OnDestroyedEvent += HandleBallDestroyed;
        }
    }

    public void SpawnBall(Vector2 position, Vector2 velocity)
    {
        //In a proper project we would have some way of spawning various types of balls, probably
        //via factories and DI. In this case the office I am working in is very cold and it is my weekend,
        //so I am just doing the boring thing and spawning a prefab :(
        BasicBall newBall = Instantiate<BasicBall>(_basicBallPrefab, position, Quaternion.identity, this.transform);
        _balls.Add(newBall);
        newBall.OnDestroyedEvent += HandleBallDestroyed;
        newBall.Initialize(velocity);
    }

    private void HandleBallDestroyed(IBall ball)
    {
        _balls.Remove(ball);
        ball.OnDestroyedEvent -= HandleBallDestroyed;
    }

    //Run this in late update so that if a ball is destroyed then created in the same frame
    //we dont end the game prematurely
    private void LateUpdate() {
        if (_balls.Count == 0)
        {
            OnAllBallsDestroyed?.Invoke();
        }
    }
    protected void UpdateBallList()
    {
        _balls = new List<IBall>(GetComponentsInChildren<IBall>());
    }
}
