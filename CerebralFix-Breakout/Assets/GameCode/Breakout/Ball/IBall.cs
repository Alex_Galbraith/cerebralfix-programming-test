using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// IBall represents the physics/backend object representing the balls
/// </summary>
public interface IBall  : IGameObject
{
    Vector2 Velocity{get;}
    event Action<IBall> OnDestroyedEvent;
    /// <summary>
    /// We allow the ball to handle this case, we may have a power up the ignores out of bounds
    /// </summary>
    void FlagOutOfBounds();
}
