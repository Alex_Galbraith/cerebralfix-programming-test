using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBallCollisionAffector
{
    BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D);
}
