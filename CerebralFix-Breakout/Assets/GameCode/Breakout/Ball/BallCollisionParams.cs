using UnityEngine;
//Using a params object because it is much robust to future changes. 
//For example if I have time, I want to add a "spin" variable to this.
//If I did not have this params struct, I would have to modify the usage of the
//relevant methods. Instead, I can just add a member to this struct
//and nothing needs to change 
public struct BallCollisionParams
{
    public Vector2 HitNormal;
    /// <summary>
    /// Represents an abstract value for the force of the collision.
    /// returning 1 will result in the ball behaving as normal, higher
    /// or lower values are up to the ball's discression to use,
    /// but will generally result in a faster or slower rebound
    /// </summary>
    public float HitPower;
    public bool CancelSpecial;

    public BallCollisionParams(Vector3 hitNormal, float hitPower) : this()
    {
        HitNormal = hitNormal;
        HitPower = hitPower;
        CancelSpecial = false;
    }
    public BallCollisionParams(RaycastHit2D raycastHit) : this()
    {
        HitNormal = raycastHit.normal;
        HitPower = 1;
        CancelSpecial = false;
    }
}