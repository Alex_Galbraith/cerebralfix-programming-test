using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// A bunch of this code is copied from my own blog post on 2d platformer controllers. I have adapted it to suit ball physiscs.
/// 
/// The reason this is not using normal unity physics and it a little complex is because I want high speed collisions to work
/// nicely for the special move.
/// 
/// Ideally the physics code in here would be extracted and we would access it via composition but this whole thing is already overengineered as hell
/// so I have left it in here
/// </summary>
public class BasicBall : MonoBehaviour, IBall
{
    [SerializeField]
    protected PlayerInputBus _playerInput;
    [SerializeField]
    protected Rigidbody2D _rigidbody2D;
    [SerializeField]
    protected Collider2D _collider;
    [SerializeField]
    protected ContactFilter2D _collisionFilter;
    [SerializeField][Tooltip("Number of times we will try to resolve collisions in a single frame")][Min(1)]
    protected int _maxMoveIterations = 5;
    [SerializeField]
    float _bounciness;
    [SerializeField]
    float _maxSpeed = 20;
    [SerializeField]
    float _specialSpeed = 100;

    [SerializeField]
    float _specialDecay = 10f;

    Vector2 _intialVel;



    //Allocate space for our hits outside the function so that
    //garbage collection doesnt have to repeatedly collect our hits
    RaycastHit2D[] hits = new RaycastHit2D[1];
    private bool _special;

    public event Action<IBall> OnDestroyedEvent;
    /// <summary>
    /// Used to trigger init animation, passes callback to be run when complete.
    /// I should really make this a single subscriber event, but its ugly as hell and
    /// I want to keep this kinda readable
    /// </summary>
    public event Action<Action> OnInitialize;
    public event Action<Vector2, RaycastHit2D> OnCollision;

    public Vector2 Velocity{get; protected set;}

    public void Initialize(Vector2 velocity)
    {
        _intialVel = velocity;
        _collider.enabled = false;
        OnInitialize?.Invoke(OnInitComplete);
    }

    private void OnInitComplete()
    {
        _collider.enabled = true;
        Velocity = _intialVel;
        _playerInput.OnSpecialTriggered += TriggerSpecial;
    }

    private void TriggerSpecial()
    {
        // _special = true;
        Velocity = Velocity.normalized * _specialSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!_special)
        {
            if(Velocity.sqrMagnitude > _maxSpeed * _maxSpeed)
            {
                Velocity -= _specialDecay * Velocity * Time.fixedDeltaTime;
            }
        }


        //How much movement do we have this frame
        Vector2 remainingDelta = Velocity * Time.fixedDeltaTime;
        int iter = 0;
        //While we have velocity remaining this frame (and iterations left), try move.
        while (remainingDelta.sqrMagnitude > 0 && iter++ < _maxMoveIterations)
        {
            CastAndMove(Velocity, remainingDelta, out Vector2 newVel, out remainingDelta, out _);
            Velocity = newVel;
        }

        
    }

    

    /// <summary>
    /// Move in the specified direction, modifying velocity if we hit something
    /// </summary>
    /// <param name="velocity">Current veloctity</param>
    /// <param name="remainingDelta">Movement to do in this frame</param>
    /// <param name="newVelocity">Output new velocity</param>
    /// <param name="newRemaining">Remaining movement this frame after moving</param>
    /// <param name="hit">Output hit</param>
    /// <param name="collisionSkin">How much should we adjust the final position by the normal direction of the hit</param>
    /// <returns>true if we need to cast again, false otherwise.</returns>
    bool CastAndMove(Vector2 velocity, Vector2 remainingDelta, out Vector2 newVelocity, out Vector2 newRemaining, out RaycastHit2D hit, float collisionSkin = 0.05f) {
        //save initial position
        Vector3 opos = _rigidbody2D.position = transform.position;

        hits[0] = new RaycastHit2D();
        //Collider cast 
        _collider.Cast(remainingDelta, _collisionFilter, hits, remainingDelta.magnitude);
        hit = hits[0];
        
        //If we didnt hit, just move
        if (!hit) {
            transform.Translate(remainingDelta);
            newRemaining = Vector3.zero;
            newVelocity = velocity;
            return false;
        }
        //We did hit
        else {
            OnCollision?.Invoke(velocity, hit);

            //Move the character
            transform.position = hit.centroid + collisionSkin * hit.normal;
            //Remove moved vel from remaining
            newRemaining = remainingDelta + (Vector2)(opos - transform.position);

            //Modify collision params
            IBallCollisionAffector affector = hit.collider.GetComponent<IBallCollisionAffector>();
            BallCollisionParams collisionParams = new BallCollisionParams(hit);
            if(affector != null)
            {
                collisionParams = affector.OnCollision(this, hit);
                _special = _special & !collisionParams.CancelSpecial;
            }


            //Bounce or slide
            newVelocity = velocity - collisionParams.HitPower * (1 + _bounciness) * (Vector2.Dot(velocity, collisionParams.HitNormal) * collisionParams.HitNormal);

            //Make remaining velocity change when hitting something
            newRemaining = newRemaining - collisionParams.HitPower * (1 + _bounciness) * Vector2.Dot(newRemaining, collisionParams.HitNormal) * collisionParams.HitNormal;
            //Check we arent going too horizontal
            float ysign = Mathf.Sign(newVelocity.y);
            if(Mathf.Abs(newVelocity.x / (newVelocity.y + ysign * 0.01f)) > 5)
            {
                //if we are too horizontal, add a fraction of our x speed to our y speed
                newVelocity = new Vector2(newVelocity.x, newVelocity.y + Mathf.Abs(newVelocity.x * 0.01f) * ysign);
            } 

            return true;
        }
    }

    public void FlagOutOfBounds()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy() {
        OnDestroyedEvent?.Invoke(this);
        _playerInput.OnSpecialTriggered -= TriggerSpecial;

    }
}
