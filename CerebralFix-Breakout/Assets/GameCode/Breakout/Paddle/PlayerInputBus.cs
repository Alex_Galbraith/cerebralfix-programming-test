using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstracts player input away from the actual input device. We can use this class to pass input to the paddle or other input reciever 
/// whether we are using mouse, touch, or even recieving multiplayer input.
/// </summary>
[CreateAssetMenu(menuName = "Player Input Bus")]
public class PlayerInputBus : ScriptableObject
{
    private Vector3 _targetPosition;
    public Vector3 TargetPosition { 
        get => _targetPosition; 
        set 
        {
            _targetPosition = value; 
            OnTargetPositionChanged?.Invoke(value);
        }
    }
    public event Action<Vector3> OnTargetPositionChanged;
    public event Action OnSpecialTriggered;

    public void TriggerSpecial()
    {
        OnSpecialTriggered?.Invoke();
    }
}
