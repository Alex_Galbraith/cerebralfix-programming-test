using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    protected PlayerInputBus _inputBus;

    [SerializeField]
    protected Camera _mainCamera;

    [SerializeField]
    [Tooltip("Max time between taps/clicks to count as a double click")]
    protected float _doubleTapDeltaS = 0.4f;

    protected float _firstTapTime;

    //When creating the component, auto locate our camera
    private void Reset() {
        _mainCamera = FindObjectOfType<Camera>();
    }

    private void Update() {
        _inputBus.TargetPosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);

        //Detect double tap for special move
        if(Input.GetMouseButtonDown(0))
        {
            //Use unscaled because it shouldnt change based on game speed
            float delta = Time.unscaledTime - _firstTapTime;
            if(delta <= _doubleTapDeltaS)
            {
                //make sure our first tap time is far enough in the past that it cant count 
                _firstTapTime = -_firstTapTime-1;
                _inputBus.TriggerSpecial();
            }
            else
            {
                _firstTapTime = Time.unscaledTime;
            }
        }
    }
}
