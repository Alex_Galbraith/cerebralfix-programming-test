using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour, IBallCollisionAffector
{
    [SerializeField]
    protected PlayerInputBus _inputBus;
    [SerializeField][Tooltip("How much the surface of the paddle is curved when hit by the ball")]
    protected float _curveScale;
    [SerializeField]
    Collider2D _mainCollider;
    [SerializeField][Tooltip("Used for confining the paddle's movement")]
    BoxCollider2D _boundsCollider;
    // Start is called before the first frame update
    private void Awake() 
    {
        _inputBus.OnTargetPositionChanged += HandleTargetPositionChanged;
        _inputBus.OnSpecialTriggered += DebugSpecial;
    }

    private void DebugSpecial()
    {
        Debug.Log("Triggered!");
    }

    private void HandleTargetPositionChanged(Vector3 pos)
    {
        float minx = _boundsCollider.bounds.min.x + _mainCollider.bounds.size.x/2f;
        float maxx = _boundsCollider.bounds.max.x - _mainCollider.bounds.size.x/2f;
        //Some terrible magic numbers because I am too lazy to make another set of boundary colliders to
        //properly confine the paddle
        float posx = Mathf.Clamp(pos.x, minx + 0.1f, maxx - 0.1f);
        //move the paddle to the mouse position
        transform.position = new Vector3(posx,transform.position.y,0);
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy() {
        _inputBus.OnTargetPositionChanged -= HandleTargetPositionChanged;
        _inputBus.OnSpecialTriggered -= DebugSpecial;
    }

    public BallCollisionParams OnCollision(IBall ball, RaycastHit2D hit2D)
    {
        float deltaX = transform.position.x - hit2D.point.x;
        Debug.Log(deltaX);
        deltaX *= _curveScale;
        Vector2 hitNorm = Quaternion.Euler(0,0,deltaX) * Vector2.down;
        //Because we are modding the normal, it is possible for the ball to try bounce into the paddle, this will prevent that
        if(Vector2.Dot(hitNorm, ball.Velocity) < 0)
            hitNorm = Vector2.down;
        Debug.Log(hitNorm);
        return new BallCollisionParams(hit2D){HitNormal = hitNorm, CancelSpecial = true};
    }
}
