using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesDisplay : MonoBehaviour
{
    [SerializeField]
    GameManager _gameManager;
    [SerializeField]
    Image _lifePrefab;
    [SerializeField]
    Sprite _emptySprite; 

    protected List<Image> _lives = new List<Image>();
    // Start is called before the first frame update
    void Start()
    {
        _gameManager.OnLivesUpdated += HandleLives;
        for (var i = 0; i < _gameManager.StartingLives; i++)
        {
            _lives.Add(Instantiate<Image>(_lifePrefab, this.transform));
        }
    }

    private void HandleLives(int lives)
    {
        for (var i = 0; i < _gameManager.StartingLives; i++)
        {
            if(i >= lives)
            {
                _lives[i].sprite = _emptySprite;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
