using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseUI : MonoBehaviour
{
    [SerializeField]
    GameManager _gameManager;
    [SerializeField]
    GameObject _mainParent;
    [SerializeField]
    GameObject _winObject;
    [SerializeField]
    GameObject _loseObject;
    [SerializeField]
    Button _restartButton; 
    // Start is called before the first frame update
    void Start()
    {
        _gameManager.OnWin += HandleWin;
        _gameManager.OnLoss += HandleLose;
        _winObject.SetActive(false);
        _loseObject.SetActive(false);
        _mainParent.SetActive(false);
        _restartButton.onClick.AddListener(HandleRestart);
    }

    private void HandleWin()
    {
        _mainParent.SetActive(true);
        _winObject.SetActive(true);
    }

    private void HandleLose()
    {
        _mainParent.SetActive(true);
        _loseObject.SetActive(true);
    }


    private void HandleRestart(){
        _gameManager.RestartLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
